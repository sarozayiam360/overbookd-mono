export interface Configuration {
  key: string;
  value: Record<string, unknown>;
}
